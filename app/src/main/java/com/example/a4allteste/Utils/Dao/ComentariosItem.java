package com.example.a4allteste.Utils.Dao;


import com.google.gson.annotations.SerializedName;

public class ComentariosItem{

	@SerializedName("urlFoto")
	private String urlFoto;

	@SerializedName("titulo")
	private String titulo;

	@SerializedName("nome")
	private String nome;

	@SerializedName("comentario")
	private String comentario;

	@SerializedName("nota")
	private int nota;

	public void setUrlFoto(String urlFoto){
		this.urlFoto = urlFoto;
	}

	public String getUrlFoto(){
		return urlFoto;
	}

	public void setTitulo(String titulo){
		this.titulo = titulo;
	}

	public String getTitulo(){
		return titulo;
	}

	public void setNome(String nome){
		this.nome = nome;
	}

	public String getNome(){
		return nome;
	}

	public void setComentario(String comentario){
		this.comentario = comentario;
	}

	public String getComentario(){
		return comentario;
	}

	public void setNota(int nota){
		this.nota = nota;
	}

	public int getNota(){
		return nota;
	}

	@Override
 	public String toString(){
		return 
			"ComentariosItem{" + 
			"urlFoto = '" + urlFoto + '\'' + 
			",titulo = '" + titulo + '\'' + 
			",nome = '" + nome + '\'' + 
			",comentario = '" + comentario + '\'' + 
			",nota = '" + nota + '\'' + 
			"}";
		}
}