package com.example.a4allteste.Utils.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a4allteste.R;
import com.example.a4allteste.Telas.Principal;
import com.example.a4allteste.Utils.Utilidade;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by willv on 26/10/2017.
 */

public class AdapString extends RecyclerView.Adapter<viewAdapString> {

    private ArrayList<String> resultsItemList;
    private Utilidade utilidade;
    private Context context;

    public AdapString(Context context, ArrayList<String> resultsItemList) {
        this.context = context;
        this.resultsItemList = resultsItemList;
        this.utilidade = new Utilidade(context);
    }

    @Override
    public viewAdapString onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_string, parent, false);
        viewAdapString viewHolder = new viewAdapString(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final viewAdapString viewHolder, final int position) {
        viewHolder.name.setText(utilidade.tratarNome(resultsItemList.get(position)));
        viewHolder.principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Principal.class);
                intent.putExtra("id", resultsItemList.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return resultsItemList.size();
    }
}

class viewAdapString extends RecyclerView.ViewHolder {

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.principal)
    RelativeLayout principal;

    viewAdapString(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}

