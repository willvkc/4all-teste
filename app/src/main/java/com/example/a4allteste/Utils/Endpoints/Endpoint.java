package com.example.a4allteste.Utils.Endpoints;

import android.content.Context;

import com.example.a4allteste.Utils.Dao.Resultado;
import com.example.a4allteste.Utils.Utilidade;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Endpoint {

    private final Context context;
    private Utilidade utilidade;
    private EndpointCall endpointCall;

    public Endpoint(Context context, EndpointCall endpointCall) {
        this.context = context;
        this.utilidade = new Utilidade(context);
        this.endpointCall = endpointCall;
    }

    public void tarefas() {
        utilidade.abrirDialog();
        Webservice service = utilidade.getRetrofit(true).create(Webservice.class);
        Call<Resultado> param = service.tarefas();
        param.enqueue(new Callback<Resultado>() {
            @Override
            public void onResponse(Call<Resultado> call, final Response<Resultado> response) {
                utilidade.fecharDialog();
                if (response.isSuccessful()) {
                    endpointCall.getResult(response.body(), 0);
                } else {
                    endpointCall.getError("Error 022: Não foi possível fazer a requisição.", 0);
                }
            }

            @Override
            public void onFailure(Call<Resultado> call, Throwable t) {
                utilidade.fecharDialog();
                endpointCall.getError("Error 022: Sem conexão", 0);
            }
        });
    }

    public void detalhes(String id) {
        utilidade.abrirDialog();
        Webservice service = utilidade.getRetrofit(false).create(Webservice.class);
        Call<Resultado> param = service.tarefas_id(id);
        param.enqueue(new Callback<Resultado>() {
            @Override
            public void onResponse(Call<Resultado> call, final Response<Resultado> response) {
                utilidade.fecharDialog();
                if (response.isSuccessful()) {
                    endpointCall.getResult(response.body(), 1);
                } else {

                    endpointCall.getError("Error 026: Não foi possível fazer a requisição.", 1);
                }

            }

            @Override
            public void onFailure(Call<Resultado> call, Throwable t) {
                utilidade.fecharDialog();
                endpointCall.getError("Error 026: Sem conexão", 1);
            }
        });
    }

    //Interface para recuperar o resultado das requests
    public interface EndpointCall {
        void getResult(Resultado resultado, int categoria);
        void getError(String mensagem, int categoria);

    }


}
