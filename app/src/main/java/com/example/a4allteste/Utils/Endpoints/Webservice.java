package com.example.a4allteste.Utils.Endpoints;


import com.example.a4allteste.Utils.Dao.Resultado;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Webservice {

    String URL = "http://dev.4all.com:3003/";

    @GET("tarefa/")
    Call<Resultado> tarefas();

    @GET("tarefa/{id}/")
    Call<Resultado> tarefas_id(@Path("id") String id);



}
