package com.example.a4allteste.Utils.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.a4allteste.R;
import com.example.a4allteste.Utils.CircleTransform;
import com.example.a4allteste.Utils.Dao.ComentariosItem;
import com.example.a4allteste.Utils.Utilidade;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by willv on 26/10/2017.
 */

public class AdapComentarios extends RecyclerView.Adapter<viewAdapComentarios> {

    private ArrayList<ComentariosItem> resultsItemList;
    private Utilidade utilidade;
    private Context context;

    public AdapComentarios(Context context, ArrayList<ComentariosItem> resultsItemList) {
        this.context = context;
        this.resultsItemList = resultsItemList;
        this.utilidade = new Utilidade(context);
    }

    @Override
    public viewAdapComentarios onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_comentario, parent, false);
        viewAdapComentarios viewHolder = new viewAdapComentarios(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final viewAdapComentarios viewHolder, final int position) {
        ComentariosItem comentariosItem = resultsItemList.get(position);
        viewHolder.nome.setText(utilidade.tratarNome(comentariosItem.getNome()));
        viewHolder.titulo.setText(comentariosItem.getTitulo());
        viewHolder.texto.setText(comentariosItem.getComentario());
        Picasso.get().load(comentariosItem.getUrlFoto()).transform(new CircleTransform()).into(viewHolder.foto);
        viewHolder.ratingBar.setRating(comentariosItem.getNota());
    }

    @Override
    public int getItemCount() {
        return resultsItemList.size();
    }
}

class viewAdapComentarios extends RecyclerView.ViewHolder {

    @BindView(R.id.nome)
    TextView nome;
    @BindView(R.id.titulo)
    TextView titulo;
    @BindView(R.id.texto)
    TextView texto;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.foto)
    ImageView foto;

    viewAdapComentarios(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}

