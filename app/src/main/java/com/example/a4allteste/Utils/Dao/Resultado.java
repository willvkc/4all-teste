package com.example.a4allteste.Utils.Dao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Resultado {

	@SerializedName("texto")
	private String texto;

	@SerializedName("cidade")
	private String cidade;

	@SerializedName("telefone")
	private String telefone;

	@SerializedName("endereco")
	private String endereco;

	@SerializedName("bairro")
	private String bairro;

	@SerializedName("urlFoto")
	private String urlFoto;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("titulo")
	private String titulo;

	@SerializedName("id")
	private String id;

	@SerializedName("urlLogo")
	private String urlLogo;

	@SerializedName("comentarios")
	private ArrayList<ComentariosItem> comentarios;

	@SerializedName("longitude")
	private double longitude;

	@SerializedName("lista")
	private ArrayList<String> lista;


	public void setTexto(String texto){
		this.texto = texto;
	}

	public String getTexto(){
		return texto;
	}

	public void setCidade(String cidade){
		this.cidade = cidade;
	}

	public String getCidade(){
		return cidade;
	}

	public void setTelefone(String telefone){
		this.telefone = telefone;
	}

	public String getTelefone(){
		return telefone;
	}

	public void setEndereco(String endereco){
		this.endereco = endereco;
	}

	public String getEndereco(){
		return endereco;
	}

	public void setBairro(String bairro){
		this.bairro = bairro;
	}

	public String getBairro(){
		return bairro;
	}

	public void setUrlFoto(String urlFoto){
		this.urlFoto = urlFoto;
	}

	public String getUrlFoto(){
		return urlFoto;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setTitulo(String titulo){
		this.titulo = titulo;
	}

	public String getTitulo(){
		return titulo;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setUrlLogo(String urlLogo){
		this.urlLogo = urlLogo;
	}

	public String getUrlLogo(){
		return urlLogo;
	}

	public void setComentarios(ArrayList<ComentariosItem> comentarios){
		this.comentarios = comentarios;
	}

	public ArrayList<ComentariosItem> getComentarios(){
		return comentarios;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	public ArrayList<String> getLista() {
		return lista;
	}

	public void setLista(ArrayList<String> lista) {
		this.lista = lista;
	}
}