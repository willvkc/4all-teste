package com.example.a4allteste.Telas;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.a4allteste.R;
import com.example.a4allteste.Utils.Adapters.AdapComentarios;
import com.example.a4allteste.Utils.Dao.Resultado;
import com.example.a4allteste.Utils.Endpoints.Endpoint;
import com.example.a4allteste.Utils.Utilidade;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Principal extends AppCompatActivity implements Endpoint.EndpointCall, OnMapReadyCallback {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.floatingActionButton)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.texto)
    TextView texto;
    @BindView(R.id.comentarios)
    RecyclerView comentarios;
    @BindView(R.id.relLigar)
    RelativeLayout relLigar;
    @BindView(R.id.relServicos)
    RelativeLayout relServicos;
    @BindView(R.id.relEnderecos)
    RelativeLayout relEnderecos;
    @BindView(R.id.relComentario)
    RelativeLayout relComentario;
    @BindView(R.id.relFavoritos)
    RelativeLayout relFavoritos;
    @BindView(R.id.endereco)
    TextView endereco;
    @BindView(R.id.scrollable)
    ScrollView scrollable;
    @BindView(R.id.mapView)
    MapView mapView;

    GoogleMap map;
    private Bundle extras;
    private Context context;
    private MenuItem menuItem;
    private Endpoint endpoint;
    private Utilidade utilidade;
    private Resultado resultado;
    private SearchView mSearchView;
    private AlertDialog dialogEndereco;
    private AlertDialog.Builder builder;
    private TextView endereco1, endereco2, txtTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_new);
        ButterKnife.bind(this);
        mapView.onCreate(savedInstanceState);

        context = this;
        extras = getIntent().getExtras();
        utilidade = new Utilidade(context);
        endpoint = new Endpoint(context, this);

        //Função que cria o titulo customizado na action bar
        ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar, null);
        txtTitle = viewActionBar.findViewById(R.id.txtTitle);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        abar.setCustomView(viewActionBar, params);

        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setHomeButtonEnabled(true);

        if (extras != null) {
            endpoint.detalhes(extras.getString("id"));
        } else {
            utilidade.mostrarToast("Error 217");
            finish();
        }

        relLigar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resultado != null) onCall();

            }
        });
        relEnderecos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resultado != null) abrirEndereco();

            }
        });

        relServicos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Servicos.class));
            }
        });

        relComentario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollable.scrollTo(0, (int) comentarios.getY());
            }
        });

    }

    public void onCall() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    123);
        } else {
            startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + resultado.getTelefone())));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case 123:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onCall();
                } else {
                    utilidade.mostrarToast("Permissão não garantida");
                }
                break;

            default:
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Função para criar o searchview
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        menuItem = menu.findItem(R.id.search);
        mSearchView = (SearchView) menuItem.getActionView();
        mSearchView.setQueryHint("Procurar...");
        return true;
    }

    //Função da interface para receber o result da request
    @Override
    public void getResult(final Resultado resul1tado, int categoria) {
        this.resultado = resul1tado;
        switch (categoria) {
            case 1:

                Picasso.get().load(resultado.getUrlFoto()).into(logo);
                Picasso.get().load(resultado.getUrlLogo()).into(floatingActionButton);
                name.setText(utilidade.tratarNome(resultado.getTitulo()));
                endereco.setText(resultado.getEndereco());
                texto.setText(resultado.getTexto());
                txtTitle.setText(resul1tado.getCidade() + " - " + resul1tado.getBairro());

                AdapComentarios adapComentarios = new AdapComentarios(context, resultado.getComentarios());
                comentarios.setLayoutManager(new LinearLayoutManager(context));
                comentarios.setItemAnimator(new DefaultItemAnimator());
                comentarios.setAdapter(adapComentarios);
                mapView.getMapAsync(this);
                break;
        }

    }

    //Função para abrir o dialgo de endereço
    private void abrirEndereco() {
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
            dialogEndereco = builder.create();
            dialogEndereco.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_up;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogLayout = inflater.inflate(R.layout.dialog_endereco, null);
            dialogEndereco.setView(dialogLayout);
            dialogEndereco.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogEndereco.requestWindowFeature(Window.FEATURE_NO_TITLE);
            endereco1 = dialogLayout.findViewById(R.id.endereco);
            endereco2 = dialogLayout.findViewById(R.id.endereco2);
        }

        utilidade.abrirDialog(dialogEndereco);
        endereco1.setText(resultado.getEndereco());
        endereco2.setText(resultado.getBairro() + ", " + resultado.getCidade());
    }


    @Override
    public void getError(String mensagem, int categoria) {
        utilidade.mostrarToast(mensagem);
    }

    //Meto para iniciar o maps com a latitude e longitude
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);

        MarkerOptions markerOptions = new MarkerOptions();
        LatLng coordenadas = new LatLng(resultado.getLatitude(), resultado.getLongitude());
        markerOptions.position(coordenadas).title("Localização")
                .snippet(resultado.getEndereco());

        map.addMarker(markerOptions);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordenadas, 10f));

    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }
}
