package com.example.a4allteste.Telas;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.a4allteste.R;
import com.example.a4allteste.Utils.Utilidade;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Servicos extends AppCompatActivity {

    @BindView(R.id.mensagem)
    TextView mensagem;
    @BindView(R.id.lista)
    RecyclerView lista;

    private Context context;
    private Utilidade utilidade;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rc_list);
        ButterKnife.bind(this);
        context = this;
        utilidade = new Utilidade(context);


        ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar, null);
        TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        txtTitle.setText("Serviços");
        abar.setCustomView(viewActionBar, params);

        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setHomeButtonEnabled(true);

        mensagem.setVisibility(View.VISIBLE);
        lista.setVisibility(View.GONE);
        mensagem.setText("Serviços");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }





}
