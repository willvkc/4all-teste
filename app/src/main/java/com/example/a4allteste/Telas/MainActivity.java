package com.example.a4allteste.Telas;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.a4allteste.R;
import com.example.a4allteste.Utils.Adapters.AdapString;
import com.example.a4allteste.Utils.Dao.Resultado;
import com.example.a4allteste.Utils.Endpoints.Endpoint;
import com.example.a4allteste.Utils.Utilidade;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements Endpoint.EndpointCall {

    @BindView(R.id.mensagem)
    TextView mensagem;
    @BindView(R.id.lista)
    RecyclerView lista;

    private Context context;
    private Utilidade utilidade;
    private Endpoint endpoint;
    private AdapString adapString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rc_list);
        ButterKnife.bind(this);
        context = this;
        utilidade = new Utilidade(context);
        endpoint = new Endpoint(context, this);
        endpoint.tarefas();

        ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar, null);
        TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        txtTitle.setText("Escolha a Tarefa");
        abar.setCustomView(viewActionBar, params);

        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setHomeButtonEnabled(true);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }


    //Função da interface para receber o result da request
    @Override
    public void getResult(Resultado resultado, int categoria) {
        switch (categoria){
            case 0:
                listar(resultado.getLista());

                break;
        }

    }

    private void listar(ArrayList list) {
        //Listar resultados com tratamento caso não venha nenhum item no array
        if (utilidade.tratarLista(mensagem, lista, list)) {
            adapString = new AdapString(context, list);
            lista.setLayoutManager(new LinearLayoutManager(context));
            lista.setItemAnimator(new DefaultItemAnimator());
            lista.setAdapter(adapString);
        }
    }

    @Override
    public void getError(String mensagem, int categoria) {
        utilidade.mostrarToast(mensagem);
    }
}
